**What does the microservice**
-------------------------

Saves and gets data from an ElasticSearch cluster. It saves any data received on the corresponding kafka topic.

-------------------------
**Compilation**

Required: java version >= 8 and maven installed

To compile the code execute `mvn clean install`. The resulted java archive is contained in the `target` folder.

-------------------------
**Environment variables**

| **Variable**           | **Description**                                                                                                  |
|------------------------|------------------------------------------------------------------------------------------------------------------|
| SPRING_PROFILES_ACTIVE | Should be "prod" to enable the production configuration with these environment variable values                   |
| ZK_NODES               | Zookeeper node list, format <host1_address>:<host1_port>,<host2_address>:<host2_port>, needed for load-balancing |
| KAFKA_BROKER_NODES     | Kafka node list, format <host1_address>:<host1_port>,<host2_address>:<host2_port>                                |
| KAFKA_DATA_TOPIC       | Name of the topic containing incoming device data that should be stored in Elastic Search                        |
| KAFKA_GROUP            | The group id for the Kafka consumer, should be the same for all io-server instances                              |
| SERVER_PORT            | The port on which the HTTP server binds                                                                          |
| ELASTIC_HOST           | Address of the Elastic Search database                                                                           |
| ELASTIC_PORT           | Port of the Elastic Search database                                                                              |

-------------------------
**Kafka message formats**

``KAFKA_DATA_TOPIC - receives the messages that come from the the devices and should be persisted``

```
{
  "deviceId": 1, // the device ID
  "timestamp": 17322332323, // the timestamp in miliseconds when the device has read the data
  "timestamp_r": 17322333323, // the timestamp in miliseconds when the message was received in the platform
  "data_source": [ //the array of objects containing the values of the sensors
    {
      "sensor_name1": { // the sensor name, can be any valid name
        "value": 34
      }
    },
    {
      "sensor_name2": {
        "value": 193
      }
    }
  ]
}
```

-------------------------
**HTTP routes**

``GET io-server/V1/data/device-data?deviceId={}&millisFrom={}&millisTo={}``

``Result``
```
[
    deviceId: long, // the device ID
    timstamp: long, // the timestamp in milliseconds of when the data was recorded by the device
    timestampR: long, // the timestamp in milliseconds of when the data was received
    values: {} // any json object
]
```

``GET io-server/V1/data/{deviceId}/device-data-count``

``Result``
```
long = the number of results for the given device id
```

## CI/CD
Compile, test, package, build docker image and push to gitlab docker registry.

## Tests
Groovy tests in `src/test/groovy`.