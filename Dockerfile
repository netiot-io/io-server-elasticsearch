FROM anapsix/alpine-java:8
ADD target/io-server-elasticsearch.jar io-server-elasticsearch.jar
ENV JAVA_OPTS=""
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /io-server-elasticsearch.jar