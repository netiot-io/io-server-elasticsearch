package io.netiot.ioserver.mappers

import spock.lang.Specification

import static io.netiot.ioserver.generators.DataEventGenerator.aDataEvent;

class DataSampleMapperSpec extends Specification {
    def 'toModel'() {
        given:
        def entity = aDataEvent()

        when:
        def result = DataSampleMapper.toModel(entity)

        then:
        result != null
        result.timestamp == entity.timestamp
    }

}
