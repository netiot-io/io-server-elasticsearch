package io.netiot.ioserver.generators

import io.netiot.ioserver.models.DataSampleModel
import com.fasterxml.jackson.databind.JsonNode

class DataSampleGenerator {
    static aDataSampleModel(Map overrides = [:]) {
        Map values = [
                deviceId  : 1L,
                timestamp : 1531055804140,
                timestampR: 1531055804140,
                values    : "[]"
        ]
        values << overrides
        return DataSampleModel.newInstance(values)
    }

}
