package io.netiot.ioserver.generators

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import io.netiot.ioserver.models.DataSampleEvent
import com.fasterxml.jackson.databind.JsonNode

class DataEventGenerator {
    static aDataEvent(Map overrides = [:]) {
        Map values = [
                deviceId  : 1L,
                timestamp : 1531055804140,
                timestampR: 1531055804140,
                values: new ArrayList<JsonNode>()
        ]
        values << overrides
        return DataSampleEvent.newInstance(values)
    }

}
