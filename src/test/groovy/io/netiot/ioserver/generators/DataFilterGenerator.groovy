package io.netiot.ioserver.generators

import io.netiot.ioserver.models.DataFilter

class DataFilterGenerator {

    static aDataFilter(Map overrides = [:]) {
        Map values = [
                deviceId  : 1L,
                millisFrom: 1530738000000,
                millisTo  : 1530738600000
        ]
        values << overrides
        return DataFilter.newInstance(values)
    }

}
