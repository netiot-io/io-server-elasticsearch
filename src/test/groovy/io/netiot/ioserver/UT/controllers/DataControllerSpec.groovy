package io.netiot.ioserver.UT.controllers

import io.netiot.ioserver.controllers.DataController
import io.netiot.ioserver.services.DataService
import org.springframework.http.ResponseEntity
import spock.lang.Specification

import static io.netiot.ioserver.generators.DataFilterGenerator.aDataFilter
import static io.netiot.ioserver.generators.DataSampleGenerator.aDataSampleModel

class DataControllerSpec extends Specification {

    def sensorService1 = Mock(DataService)
    def sensorService2 = Mock(DataService)
    def dataController1 = new DataController(sensorService1)
    def dataController2 = new DataController(sensorService2)

    def 'getData'(){
        given:
        def dataFilter = aDataFilter()
        def sensorDataModel = aDataSampleModel()

        when:
        def result = dataController1.getDeviceData(dataFilter.getDeviceId(), dataFilter.getMillisFrom(),
                dataFilter.getMillisTo())

        then:
        1 * sensorService1.getDataSample(dataFilter) >> [sensorDataModel]
        0 * _

        result == [sensorDataModel]
    }

    def 'getDataWithoutEnd'(){
        given:
        def dataFilter = aDataFilter()
        def sensorDataModel = aDataSampleModel()

        when:
        def result = dataController2.getDeviceData(dataFilter.getDeviceId(), dataFilter.getMillisFrom(),
                dataFilter.getMillisTo())

        then:
        1 * sensorService2.getDataSample(dataFilter) >> [sensorDataModel]
        0 * _

        result == [sensorDataModel]
    }
}
