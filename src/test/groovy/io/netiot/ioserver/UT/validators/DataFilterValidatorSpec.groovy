package io.netiot.ioserver.UT.validators

import io.netiot.ioserver.exceptions.DataException
import io.netiot.ioserver.validators.DataFilterValidator
import spock.lang.Specification

import static io.netiot.ioserver.generators.DataFilterGenerator.aDataFilter

class DataFilterValidatorSpec extends Specification {

    def dataFilterValidator =  new DataFilterValidator()

    def 'validate'(){
        given:
        def dataFilter = aDataFilter()

        when:
        dataFilterValidator.validate(dataFilter)

        then:
        0 * _

    }

    def 'validate thrown DataException - millisFrom greater then millisTo'(){
        given:
        def  millisFrom = 1930738000000
        def millisTo = 1530738600000
        def dataFilter = aDataFilter(millisFrom: millisFrom, millisTo: millisTo)

        when:
        dataFilterValidator.validate(dataFilter)

        then:
        0 * _
        thrown(DataException)
    }

}
