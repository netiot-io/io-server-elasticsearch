package io.netiot.ioserver.UT.services

import io.netiot.ioserver.services.KafkaMessageReceiverService
import io.netiot.ioserver.services.DataService
import spock.lang.Specification

import static io.netiot.ioserver.generators.DataEventGenerator.aDataEvent

class KafkaMessageReceiverServiceSpec extends Specification {

    def sensorService = Mock(DataService)
    def kafkaMessageReceiverService = new KafkaMessageReceiverService(sensorService)

    def 'receiveEvent DataEvent'(){
        given:
        def dataEvent = aDataEvent()

        when:
        kafkaMessageReceiverService.receiveEvent(dataEvent)

        then:
        1 * sensorService.saveData(dataEvent)
        0 * _
    }

}
