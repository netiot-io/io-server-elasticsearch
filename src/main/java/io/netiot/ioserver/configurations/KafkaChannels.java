package io.netiot.ioserver.configurations;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.MessageChannel;

public interface KafkaChannels {

    String DATA_INPUT = "data";

    @Input(KafkaChannels.DATA_INPUT)
    MessageChannel dataInput();

}
