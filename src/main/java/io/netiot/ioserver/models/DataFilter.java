package io.netiot.ioserver.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataFilter {

    @NotNull
    private Long deviceId;

    @NotNull
    private Long millisFrom;

    @NotNull
    private Long millisTo;

}
