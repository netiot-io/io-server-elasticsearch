package io.netiot.ioserver.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.fasterxml.jackson.databind.JsonNode;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataSampleModel {

    private Long deviceId;

    private Long timestamp;

    private Long timestampR;

    private String values;

}
