package io.netiot.ioserver.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
//@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class DataSampleEvent {
    private Long deviceId;

    private Long timestamp;

    @JsonProperty("timestamp_r")
    private Long timestampR;

    @JsonProperty("data_source")
    private List<JsonNode> values;

    public String valuesString() {
        ObjectMapper mapper = new ObjectMapper();
        return "[" + values.stream().map(
                x -> {
                    try {
                        return mapper.writeValueAsString(x);
                    } catch (Exception ex) {
                        log.error("An error occurred while serializing the value array: " + ex.toString());
                        throw new RuntimeException(ex);
                    }
                }
        ).reduce((x, y) -> x  + "," + y).orElse("") + "]";
    }
}
