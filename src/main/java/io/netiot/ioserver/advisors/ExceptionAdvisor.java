package io.netiot.ioserver.advisors;

import io.netiot.ioserver.exceptions.DataException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;

@RestControllerAdvice
public final class ExceptionAdvisor {

    @ExceptionHandler(value = {DataException.class})
    public ResponseEntity<HashMap> errorHandle(final DataException dataException) {
        HashMap<String, String> body = new HashMap<>();
        body.put("error", dataException.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(body);
    }

}