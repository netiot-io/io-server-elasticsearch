package io.netiot.ioserver.services;

import io.netiot.ioserver.mappers.DataSampleMapper;
import io.netiot.ioserver.models.DataSampleEvent;
import io.netiot.ioserver.models.DataFilter;
import io.netiot.ioserver.models.DataSampleModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.*;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.core.CountRequest;
import org.elasticsearch.client.core.CountResponse;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.io.IOException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import com.fasterxml.jackson.databind.*;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class DataService {
    @Autowired
    private final RestHighLevelClient client;

    private final DateTimeFormatter indexDateFormat = DateTimeFormatter.ofPattern("yyyy.MM");

    public void saveData(final DataSampleEvent dataSampleEvent) {
        ZonedDateTime now = ZonedDateTime.now(ZoneOffset.UTC);

        try {
            IndexRequest indexRequest = new IndexRequest("sensordata-" + now.format(indexDateFormat))
                                            .type("data")
                                            .opType(DocWriteRequest.OpType.INDEX)
                                            .source(new ObjectMapper().writeValueAsString(dataSampleEvent), XContentType.JSON);

            client.index(indexRequest, RequestOptions.DEFAULT);

        } catch (IOException e) {
            log.error("An error occurred while saving incoming data: " + e.toString());
        }
    }

    public List<DataSampleModel> getDataSample(final DataFilter searchSensorData) {
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery()
                .filter(QueryBuilders.termQuery("deviceId", searchSensorData.getDeviceId()))
                .filter(QueryBuilders.rangeQuery("timestamp")
                                        .from(searchSensorData.getMillisFrom())
                                        .to(searchSensorData.getMillisTo()));

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder()
                                                    .sort("timestamp", SortOrder.ASC)
                                                    .size(1000)
                                                    .query(boolQueryBuilder);

        Scroll scroll = new Scroll(new TimeValue(60000));

        SearchRequest searchRequest = new SearchRequest()
                .indices("sensordata*")
                .source(searchSourceBuilder)
                .scroll(scroll);

        try {
            SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
            List<DataSampleModel> results = new ArrayList<>();

            String scrollId = searchResponse.getScrollId();
            SearchHit[] searchHits = searchResponse.getHits().getHits();

            ObjectMapper mapper = new ObjectMapper();

            while (searchHits != null && searchHits.length > 0) {
                for (SearchHit hit : searchHits) {

                    results.add(DataSampleMapper.toModel(mapper.readValue(hit.getSourceAsString(), DataSampleEvent.class)));
                }

                SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
                scrollRequest.scroll(scroll);
                searchResponse = client.scroll(scrollRequest, RequestOptions.DEFAULT);
                scrollId = searchResponse.getScrollId();
                searchHits = searchResponse.getHits().getHits();
            }

            ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
            clearScrollRequest.addScrollId(scrollId);
            client.clearScroll(clearScrollRequest, RequestOptions.DEFAULT);

            return results;
        } catch (IOException e) {
            log.error("An error occurred while searching the data for device " +
                    searchSensorData.getDeviceId() + ": " + e.toString());
        }

        return new ArrayList<>();
    }

    public Long getDeviceDataCount(final Long deviceId) {
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder()
                                                    .query(QueryBuilders.termQuery("deviceId", deviceId));
        CountRequest countRequest = new CountRequest()
                .indices("sensordata*")
                .source(searchSourceBuilder);

        try {
            CountResponse countResponse = client.count(countRequest, RequestOptions.DEFAULT);

            return countResponse.getCount();
        } catch (IOException e) {
            log.error("An error occurred while counting the data for device " +
                    deviceId + ": " + e.toString());
        }

        return 0L;
    }
}
