package io.netiot.ioserver.controllers;

import io.netiot.ioserver.models.DataFilter;
import io.netiot.ioserver.models.DataSampleModel;
import io.netiot.ioserver.services.DataService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Slf4j
@RestController
@RequestMapping("/data")
@RequiredArgsConstructor
public class DataController {

    private final DataService dataService;

    @GetMapping("/device-data")
    public List<DataSampleModel> getDeviceData(@RequestParam(name = "deviceId") final Long deviceId,
                                               @RequestParam(name = "millisFrom") final Long millisFrom,
                                               @RequestParam(name = "millisTo") final Long millisTo) {
        return dataService.getDataSample(getDataFilter(deviceId, millisFrom, millisTo));
    }

    @GetMapping("/{deviceId}/device-data-count")
    public Long getDeviceDataCount(@PathVariable(name = "deviceId") final Long deviceId) {
        return dataService.getDeviceDataCount(deviceId);
    }

    private DataFilter getDataFilter(final Long deviceId, final Long millisFrom, final Long millisTo) {
        return DataFilter.builder()
                .deviceId(deviceId)
                .millisFrom(millisFrom)
                .millisTo(millisTo == null ? System.currentTimeMillis() : millisTo)
                .build();
    }

}
