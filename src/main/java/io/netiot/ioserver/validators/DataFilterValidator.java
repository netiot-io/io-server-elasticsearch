package io.netiot.ioserver.validators;

import io.netiot.ioserver.exceptions.DataException;
import io.netiot.ioserver.models.DataFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static io.netiot.ioserver.util.ErrorMessages.IO_SERVER_MILLIS_FROM_GREATER_THAN_MILLIS_TO_ERROR_CODE;

@Slf4j
@Component
public class DataFilterValidator {

    public void validate(final DataFilter dataFilter) {
        if(dataFilter.getMillisFrom() > dataFilter.getMillisTo()){
            throw new DataException(IO_SERVER_MILLIS_FROM_GREATER_THAN_MILLIS_TO_ERROR_CODE);
        }
    }

}
