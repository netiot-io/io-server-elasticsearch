package io.netiot.ioserver.mappers;

import io.netiot.ioserver.models.DataSampleEvent;
import io.netiot.ioserver.models.DataSampleModel;

public final class DataSampleMapper {

    private DataSampleMapper() {
    }

    public static DataSampleModel toModel(final DataSampleEvent dataSampleEvent){
        return DataSampleModel.builder()
                .deviceId(dataSampleEvent.getDeviceId())
                .timestamp(dataSampleEvent.getTimestamp())
                .timestampR(dataSampleEvent.getTimestampR())
                .values(dataSampleEvent.valuesString())
                .build();
    }

}
