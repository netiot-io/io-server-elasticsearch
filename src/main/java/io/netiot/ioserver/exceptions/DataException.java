package io.netiot.ioserver.exceptions;

public class DataException extends RuntimeException {

    public DataException() {
    }

    public DataException(final String message) {
        super(message);
    }
}
